import MovieDetailsTable from "./DetailsTable";

function DetailsModal(props: any) {
  return (
    <>
      <div className={`modal ${props.modalOpen}`}>
        <div className="modal-box relative">
          <label
            htmlFor="my-modal-3"
            className="btn btn-sm btn-circle absolute right-2 top-2"
            onClick={props.onCloseDetailsModal}
          >
            ✕
          </label>
          <h3 className="text-lg font-bold mb-5">
            {props.movie.title || props.movie.name} (
            {props.movie.release_date?.split("-")[0] ||
              props.movie.first_air_date?.split("-")[0]}
            )
          </h3>
          <img
            className="rounded-t-lg max-h-96 m-auto"
            src={`https://image.tmdb.org/t/p/w500/${props.movie.poster_path}`}
            alt="Movie Poster"
          />
          <p className="py-4">{props.movie.overview}</p>
          <MovieDetailsTable movie={props.movie} />
        </div>
      </div>
      {/* <React.Fragment>
        <Modal
          dismissible={true}
          show={props.show}
          onClose={props.onCloseDetailsModal}
        >
          <Modal.Header>
            {props.movie.title} ({props.movie.release_date?.split("-")[0]})
          </Modal.Header>
          <Modal.Body>
            <div className="space-y-6 flex flex-col items-center">
              <img
                className={`rounded-t-lg max-h-96`}
                src={`https://image.tmdb.org/t/p/w500/${props.movie.poster_path}`}
                alt="Movie Poster"
              />
              <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                {props.movie.overview}
              </p>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <MovieDetailsTable movie={props.movie} />
          </Modal.Footer>
        </Modal>
      </React.Fragment> */}
    </>
  );
}

export default DetailsModal;
