import dayjs from "dayjs";

function MovieDetailsTable(props: any) {
  return (
    <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <tbody>
          <tr className="border-b border-gray-200 dark:border-gray-700">
            <td className="px-6 py-4">Original Language</td>
            <td className="px-6 py-4 bg-gray-50 dark:bg-gray-800">
              {props.movie.original_language}
            </td>
          </tr>
          <tr className="border-b border-gray-200 dark:border-gray-700">
            <td className="px-6 py-4">Release Date</td>
            <td className="px-6 py-4 bg-gray-50 dark:bg-gray-800">
              {dayjs(props.movie.release_date).format('DD MMMM, YYYY')}
            </td>
          </tr>
          <tr className="border-b border-gray-200 dark:border-gray-700">
            <td className="px-6 py-4">Vote Count</td>
            <td className="px-6 py-4 bg-gray-50 dark:bg-gray-800">
              {props.movie.vote_count}
            </td>
          </tr>
          <tr className="border-b border-gray-200 dark:border-gray-700">
            <td className="px-6 py-4">Vote Average</td>
            <td className="px-6 py-4 bg-gray-50 dark:bg-gray-800">
              {props.movie.vote_average}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default MovieDetailsTable;
