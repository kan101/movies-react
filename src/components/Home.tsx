import { useEffect, useState } from "react";
import MovieCard from "./MovieCard";
import Pagination from "./Pagination";
import DetailsModal from "./movie-details/DetailsModal";
import Search from "./Search";
import DiscoverFilter from "./filters/DiscoverFilter";
import DiscoverTitle from "./DiscoverTitle";

function Home() {
  const [movieList, setMovieList] = useState([]);
  const [pages, setPages] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  const [movie, setMovie] = useState({});
  const [showDetailsModal, setShowDetailsModal] = useState("");
  const [searchTerm, setSearchTerm] = useState("");

  const [formData, setFormData] = useState({
    release_date_lte: new Date().getFullYear(),
    release_date_gte: "1960",
    sort_by: "popularity.desc",
    vote_average_lte: "",
    vote_average_gte: "6",
    language: "en-US",
    isMovie: true,
  });

  useEffect(() => {
    console.log(formData.isMovie)
    const baseUrl = "https://api.themoviedb.org/3/";
    const apiKey = "b9393f02ad115189091e683c2f6e4559";
    const searchType = searchTerm === "" ? "discover" : "search";
    const media = formData.isMovie ? 'movie' : 'tv';

    const url = `${baseUrl}${searchType}/${media}?api_key=${apiKey}&query=${searchTerm}&language=${formData.language}&include_video=true&sort_by=${formData.sort_by}&page=${pageNumber}&vote_average.gte=${formData.vote_average_gte}&vote_average.lte=${formData.vote_average_lte}&release_date.lte=${formData.release_date_lte}&release_date.gte=${formData.release_date_gte}&first_air_date.gte=${formData.release_date_gte}&first_air_date.lte=${formData.release_date_lte}`;

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setMovieList(data.results);
        setPages(data.total_pages > 500 ? 500 : data.total_pages);
      });
  }, [pageNumber, searchTerm, formData]);

  const moviesElement = movieList?.map((movie: any) => {
    return (
      <MovieCard
        movie={movie}
        handleOpenDetailsModal={handleOpenDetailsModal}
        key={movie.id}
      />
    );
  });

  function handlePageClick(pageNumber: number) {
    setPageNumber(pageNumber);
  }

  function handleOpenDetailsModal(movie: any) {
    setMovie(movie);
    setShowDetailsModal("modal-open");
  }

  function handleCloseDetailsModal() {
    setShowDetailsModal("");
  }
 
  function handleSearch(search: string) {
    setSearchTerm(search);
  }

  function handleFilter(event: any) {
    const { name, value, type, checked } = event.target;
    console.log(name, value, type, checked, event, 111);
    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        [name]: type === 'checkbox' ? checked : value,
      };
    });
  }

  function handleResetFilter() {
    setFormData((prevFormData) => {
      return {
        ...prevFormData, // isMovie isn't reset
        release_date_lte: new Date().getFullYear(),
        release_date_gte: "1960",
        sort_by: "popularity.desc",
        vote_average_lte: "",
        vote_average_gte: "6",
        language: "en-US",
      };
    });
  }

  return (
    <>
      <DiscoverTitle formData={formData} onFilter={handleFilter} />
      <section className="flex flex-wrap text-lg">
        <Search onSearch={handleSearch} />
        {searchTerm === "" && (
          <DiscoverFilter
            onResetFilter={handleResetFilter}
            formData={formData}
            onFilter={handleFilter}
          />
        )}
      </section>
      <section className="flex flex-wrap">{moviesElement}</section>
      <DetailsModal
        onCloseDetailsModal={handleCloseDetailsModal}
        modalOpen={showDetailsModal}
        movie={movie}
      />
      <Pagination onPageClick={handlePageClick} pages={pages} />
    </>
  );
}

export default Home;
