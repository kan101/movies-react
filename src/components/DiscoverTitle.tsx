function DiscoverTitle(props: any) {
  return (
    <form className="flex justify-center">
      <div className="form-control">
        <label className="label cursor-pointer">
          <input type="checkbox" onChange={(event) => props.onFilter(event) } checked={props.formData.isMovie} name="isMovie" id="isMovie" className="toggle mr-5" />
          <h2 className="text-3xl main-title">{props.formData.isMovie ? "Movies" : "Television"}</h2>
        </label>
      </div>
    </form>
  );
}

export default DiscoverTitle;
