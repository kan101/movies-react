import Stars from "./movie-details/Stars";

function MovieCard(props: any) {
  const overview =
    props.movie.overview.length > 200
      ? props.movie.overview.slice(0, 200) + "..."
      : props.movie.overview;
  const imgSrc = props.movie.backdrop_path
    ? props.movie.backdrop_path
    : props.movie.poster_path;
  return (
    <div
      onClick={() => props.handleOpenDetailsModal(props.movie)}
      className="cursor-pointer mr-4 mb-5 movie-card max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700"
    >
      <a href="#" className={`card-image-wrapper rounded-t-lg`}>
        <img
          className={`card-image`}
          src={`https://image.tmdb.org/t/p/w500/${imgSrc}`}
          alt=""
        />
      </a>
      <div className="p-5">
        <a href="#" className="flex items-center">
          <h5 className="mb-2 text-lg tracking-tight text-gray-900 dark:text-white">
            <span>{props.movie.title || props.movie.name}</span>
            <span className="mx-3">
              ({props.movie.release_date?.split("-")[0] || props.movie.first_air_date.split("-")[0]})
            </span>
            <span className="badge badge-secondary">{props.movie.vote_average}</span>
            <span>
              <Stars movie={props.movie} />
            </span>
          </h5>
        </a>
        <p className="mb-3 text-left font-normal text-gray-700 dark:text-gray-400">
          {overview}
        </p>
      </div>
    </div>
  );
}

export default MovieCard;
