import { DebounceInput } from "react-debounce-input";

function Search(props: any) {
  return (
    <div className="collapse w-72">
      <input type="checkbox" />
      <div className="collapse-title w-28 font-medium">Search</div>
      <div className="collapse-content">
        <form>
          <div className="relative z-0 mb-6 group">
            <DebounceInput
              debounceTimeout={500}
              type="text"
              name="search"
              id="search"
              className="block py-2.5 pl-3 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
              placeholder="Search Movies"
              onChange={(event) => props.onSearch(event.target.value)}
            />
          </div>
        </form>
      </div>
    </div>
  );
}

export default Search;
