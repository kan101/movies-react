import { useEffect, useState } from "react";
import { DebounceInput } from "react-debounce-input";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface Language {
  english_name: string;
  iso_639_1: string;
  name: string;
}

function DiscoverFilter(props: any) {
  const [languages, setLanguages] = useState([]);
  const [yearFrom, setYearFrom] = useState(
    new Date(`${props.formData.release_date_gte}`)
  );
  const [yearTo, setYearTo] = useState(new Date());

  useEffect(() => {
    const baseUrl = "https://api.themoviedb.org/3/";
    const apiKey = "b9393f02ad115189091e683c2f6e4559";
    const url = `${baseUrl}/configuration/languages?api_key=${apiKey}`;
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        let sortedArray = data.sort((a: Language, b: Language) =>
          a.english_name.localeCompare(b.english_name)
        );
        setLanguages(sortedArray);
      });
  }, []);

  const LanguageOptions = languages?.map((language: any, index: number) => {
    return (
      <option key={`language.english_name_${index}`} value={language.iso_639_1}>
        {language.english_name}
      </option>
    );
  });

  function handleYearSelect(date: any, event: any, name: string) {
    name === "release_date_gte" ? setYearFrom(date) : setYearTo(date);
    props.onFilter({
      ...event,
      target: {
        ...event.target,
        // added because the event from date picker doesn't have these properties
        value: date.getFullYear(),
        name: name,
      },
    });
  }

  return (
    <div className="collapse filter w-1/2">
      <input type="checkbox" />
      <div className="collapse-title w-28 font-medium">Filter</div>
      <div className="collapse-content">
        <form>
          <div className="grid gap-6 mb-6 md:grid-cols-2">
            <div>
              <label
                htmlFor="release_date_gte"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Year from
              </label>
              <DatePicker
                selected={yearFrom}
                onChange={(date: any, event) =>
                  handleYearSelect(date, event, "release_date_gte")
                }
                showYearPicker
                dateFormat="yyyy"
                value={props.formData.release_date_gte}
                name="release_date_gte"
                id="release_date_gte"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              />
            </div>
            <div>
              <label
                htmlFor="release_date_lte"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Year to
              </label>
              <DatePicker
                selected={yearTo}
                onChange={(date: any, event) =>
                  handleYearSelect(date, event, "release_date_lte")
                }
                showYearPicker
                dateFormat="yyyy"
                value={props.formData.release_date_lte}
                name="release_date_lte"
                id="release_date_lte"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              />
            </div>
            <div>
              <label
                htmlFor="min_rating"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Min Rating
              </label>
              <DebounceInput
                debounceTimeout={1000}
                onChange={(event) => props.onFilter(event)}
                type="text"
                id="min_rating"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Minimum rating"
                name="vote_average_gte"
                value={props.formData.vote_average_gte}
              />
            </div>
            <div>
              <label
                htmlFor="max_rating"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Max rating
              </label>
              <DebounceInput
                debounceTimeout={1000}
                onChange={(event) => props.onFilter(event)}
                type="text"
                id="max_rating"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Maximum rating"
                name="vote_average_lte"
                value={props.formData.vote_average_lte}
              />
            </div>
            <div>
              <label
                htmlFor="sort_by"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Sort By
              </label>
              <select
                id="sort_by"
                onChange={(event) => props.onFilter(event)}
                name="sort_by"
                value={props.formData.sort_by}
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              >
                <option value="popularity.desc">Popularity</option>
                <option value="revenue.desc">Revenue</option>
                <option value="vote_average.desc">Vote Average</option>
                <option value="release_date.desc">Release Date</option>
              </select>
            </div>
            <div>
              <label
                htmlFor="language"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Language
              </label>
              <select
                name="language"
                value={props.formData.language}
                id="language"
                onChange={(event) => props.onFilter(event)}
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              >
                {LanguageOptions}
              </select>
            </div>
          </div>
        </form>
        <button className="btn btn-sm" onClick={props.onResetFilter}>Reset Filter</button>
      </div>
    </div>
  );
}

export default DiscoverFilter;
