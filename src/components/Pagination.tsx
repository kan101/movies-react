import ReactPaginate from "react-paginate";

function Pagination(props: any) {
  const pageClassName = `pagination cursor-pointer block ml-0 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`
  return (
    <ReactPaginate
      breakLabel="..."
      nextLabel="next"
      onPageChange={(page) => {props.onPageClick(page.selected === 0 ? 1 : page.selected+1)}}
      pageRangeDisplayed={5}
      pageCount={props.pages}
      previousLabel="previous"
      renderOnZeroPageCount={null}
      containerClassName={`inline-flex items-center -space-x-px flex-wrap`}
      pageClassName={pageClassName}
      previousClassName={`pagination rounded-l-lg ${pageClassName}`}
      nextClassName={`pagination rounded-r-lg ${pageClassName}`}
      activeClassName={`active-page ${pageClassName}`}
      breakClassName={pageClassName}
    />
  );
}

export default Pagination;
